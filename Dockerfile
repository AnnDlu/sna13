FROM python:3.11.0-alpine3.15

WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY main.py .
COPY test.py .
CMD python main.py